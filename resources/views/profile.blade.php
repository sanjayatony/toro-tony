@extends('layouts.app')

@section('title', 'Homepage')

@section('header')
	<h1 class="title">Profile</h1>
@endsection

@section('content')
	@if (Session::has('message'))
		<div class="notification is-success">
	  	<button class="delete"></button>
			{{ Session::get('message') }}
		</div>
	@endif
	@if (Session::has('message-error'))
		<div class="notification is-danger">
	  	<button class="delete"></button>
			{{ Session::get('message-error') }}
		</div>
	@endif
	@if (count($errors) > 0)
     <div class = "notification is-danger">
        <ul>
           @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
           @endforeach
        </ul>
     </div>
  @endif

	<form method="POST" action="/update">
    @csrf

		<div class="field">
		  <label class="label">First Name</label>
		  <div class="control">
		    <input class="input" type="text" name="fname" value="{{ $data->first_name }}">
		  </div>
		</div>

		<div class="field">
		  <label class="label">Last Name</label>
		  <div class="control">
		    <input class="input" type="text" name="lname" value="{{ $data->last_name }}">
		  </div>
		</div>
		<div class="field">
		  <label class="label">Time Zone</label>
		  <div class="control">
		    <input class="input" type="text" name="timezone" value="{{ $data->timezone }}">
		  </div>
		</div>

		<div class="field">
		  <div class="control">
		    <button class="button is-primary">Submit</button>
		  </div>
		</div>
	</form>


@endsection
