<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  	<title>Toro Tony - @yield('title')</title>
  	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.css">
  </head>
  <body>
  	<section class="hero is-primary">
		  <div class="hero-body">
		    <div class="container">
		      @yield('header')
		    </div>
		  </div>
		</section>

    <section class="section">
    	<div class="container">
        <div class="columns">
         @if(Session::get('token'))
          <div class="column is-one-quarter">
            <p>Hi {{ Session::get('first_name') }} {{ Session::get('last_name') }}</p>
            <hr />
            <ul>
              <li><a href="/profile">Profile</a></li>
              <li><a href="/change-password">Change Password</a></li>
               <li><a href="/logout">Logout</a></li>
            </ul>
          </div>
          @endif
          <div class="column">
            @yield('content')
          </div>
        </div>

      </div>
    </section>
  </body>
</html>
