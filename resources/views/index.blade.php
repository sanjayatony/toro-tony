@extends('layouts.app')

@section('title', 'Homepage')

@section('header')
	<h1 class="title">Hello World</h1>
  <h2 class="subtitle">Welcome to Toro Bravo!</h2>
@endsection

@section('content')
	<div class="well">
		<form method="POST" action="/update">
	    @csrf

			<div class="field">
			  <label class="label">First Name</label>
			  <div class="control">
			    <input class="input" type="text" name="fname" value="{{ $data->first_name }}">
			  </div>
			</div>

			<div class="field">
			  <label class="label">Last Name</label>
			  <div class="control">
			    <input class="input" type="text" name="lname" value="{{ $data->last_name }}">
			  </div>
			</div>
			<div class="field">
			  <label class="label">Time Zone</label>
			  <div class="control">
			    <input class="input" type="text" name="timezone" value="{{ $data->timezone }}">
			  </div>
			</div>
			<hr />
			<p>Leave blank, if you dont want to change the password.</p>

			<div class="field">
			  <label class="label">Current Password</label>
			  <div class="control">
			    <input class="input" type="password" name="password">
			  </div>
			</div>
			<div class="field">
			  <label class="label">New Password</label>
			  <div class="control">
			    <input class="input" type="password" name="new_password">
			  </div>
			</div>
			<div class="field">
			  <label class="label">Confirm New Password</label>
			  <div class="control">
			    <input class="input" type="password" name="confirm_password">
			  </div>
			</div>

			<div class="field">
			  <div class="control">
			    <button class="button is-primary">Submit</button>
			  </div>
			</div>
		</form>
	</div>
@endsection
