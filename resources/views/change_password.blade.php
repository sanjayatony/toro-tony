@extends('layouts.app')

@section('title', 'Homepage')

@section('header')
	<h1 class="title">Change Password</h1>
@endsection

@section('content')
	@if (Session::has('message'))
		<div class="notification is-success">
	  	<button class="delete"></button>
			{{ Session::get('message') }}
		</div>
	@endif
	@if (Session::has('message-error'))
		<div class="notification is-danger">
	  	<button class="delete"></button>
			{{ Session::get('message-error') }}
		</div>
	@endif
	@if (count($errors) > 0)
     <div class = "notification is-danger">
        <ul>
           @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
           @endforeach
        </ul>
     </div>
  @endif

	<form method="POST" action="/updatePassword">
    @csrf

		<div class="field">
		  <label class="label">Current Password</label>
		  <div class="control">
		    <input class="input" type="password" name="password">
		  </div>
		</div>
		<div class="field">
		  <label class="label">New Password</label>
		  <div class="control">
		    <input class="input" type="password" name="new_password">
		  </div>
		</div>
		<div class="field">
		  <label class="label">Confirm New Password</label>
		  <div class="control">
		    <input class="input" type="password" name="new_password_confirmation">
		  </div>
		</div>

		<div class="field">
		  <div class="control">
		    <button class="button is-primary">Submit</button>
		  </div>
		</div>
	</form>

@endsection
