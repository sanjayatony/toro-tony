@extends('layouts.app')

@section('title', 'Homepage')

@section('header')
	<h1 class="title">Login</h1>
@endsection
@section('content')
	@if (Session::has('message-error'))
		<div class="notification is-danger">
	  	<button class="delete"></button>
			{{ Session::get('message-error') }}
		</div>
	@endif
	@if (count($errors) > 0)
     <div class = "notification is-danger">
        <ul>
           @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
           @endforeach
        </ul>
     </div>
  @endif
	<form method="POST" action="/login">
    @csrf

		<div class="field">
		  <label class="label">Email</label>
		  <div class="control">
		    <input class="input" type="text" name="email">
		  </div>
		</div>

		<div class="field">
		  <label class="label">Password</label>
		  <div class="control">
		    <input class="input" type="password" name="password">
		  </div>
		</div>

		<div class="field">
		  <div class="control">
		    <button class="button is-primary">Submit</button>
		  </div>
		</div>
	</form>
@endsection
