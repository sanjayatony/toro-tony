<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Session;
use Redirect;

class UsersController extends Controller
{
    //
	public function profile()
	{
		if(!Session::get('token')){
    	return redirect('login')->with('alert','You should login first.');
    }
    else{
    	$token = 'Bearer ' .Session::get('token');
    	$client = new \GuzzleHttp\Client(['base_uri' => 'https://shaiful.info/api/']);
    	$headers = [
    		//'Accept'        => 'application/json',
    		//'Content-Type'	=> 'application/x-www-form-urlencoded',
    		'Authorization' => $token
			];
			$res = $client->request('POST', 'user', ['headers' => $headers]);
			$data =json_decode($res->getbody());
			Session::put('first_name',$data->first_name);
			Session::put('last_name',$data->last_name);
      return view('profile')->with('data', $data);
    }
	}

	public function changePassword()
	{
		if(!Session::get('token')){
    	return redirect('login')->with('alert','You should login first.');
    }
    else{
    	$token = 'Bearer ' .Session::get('token');
    	$client = new \GuzzleHttp\Client(['base_uri' => 'https://shaiful.info/api/']);
    	$headers = [
    		//'Accept'        => 'application/json',
    		//'Content-Type'	=> 'application/x-www-form-urlencoded',
    		'Authorization' => $token
			];
			$res = $client->request('POST', 'user', ['headers' => $headers]);
			$data =json_decode($res->getbody());
      return view('change_password')->with('data', $data);
    }
	}

	public function update(Request $request)
	{
		$token = 'Bearer ' .Session::get('token');
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://shaiful.info/api/']);
  	$headers = [
  		'Accept'        => 'application/json',
  		'Content-Type'	=> 'application/x-www-form-urlencoded',
  		'Authorization' => $token
		];

		$this->validate($request, [
			'fname' => 'required',
			'lname' => 'required',
			'timezone' => 'required'
		]);

		$fname = $request->input('fname');
		$lname = $request->input('lname');
		$timezone = $request->input('timezone');

		$res = $client->request('POST', 'user/profile',[
			'query' => [
				'first_name' => $fname,
				'last_name' => $lname,
				'timezone' => $timezone
			],
			'headers' => $headers
		]);

		$status = json_decode($res->getbody())->status;
		if($status == 'OK'){
			Session::flash('message', 'Profile updated');
		}else{
			Session::flash('message-error', 'Updating failed');
		}
		return redirect('profile');


	}

	public function updatePassword(Request $request)
	{
		$token = 'Bearer ' .Session::get('token');
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://shaiful.info/api/']);
  	$headers = [
  		'Accept'        => 'application/json',
  		'Content-Type'	=> 'application/x-www-form-urlencoded',
  		'Authorization' => $token
		];


		$this->validate($request, [
			'password' => 'required',
			'new_password' => 'required|min:6|different:password',
			'new_password_confirmation' => 'required|min:6|same:new_password'
		]);

		$passwd = $request->input('password');
		$new_passwd = $request->input('new_password');
		$c_passwd = $request->input('new_password_confirmation');



		$res = $client->request('POST', 'user/password',[
			'query' => [
				'current_password' => $passwd,
				'new_password' => $new_passwd,
				'new_password_confirmation' => $c_passwd
			],
			'headers' => $headers
		]);
		$status = json_decode($res->getbody())->status;

		if($status == 'OK'){
			Session::flash('message', 'Password updated');
			return redirect('change-password');
		}else{
			Session::flash('message-error', 'Current Password not match');
			return redirect('change-password');
		}
	}

	public function login()
	{
		return view('login');
	}

	public function checkLogin(Request $request)
	{
		//dd(request()->all());
		$email = $request->input('email');
		$password = $request->input('password');
		$client = new \GuzzleHttp\Client(['base_uri' => 'https://shaiful.info/api/']);

		$this->validate($request, [
			'email' => 'required|email',
			'password' => 'required',
		]);

		$res = $client->request('POST', 'login', [
			'query' => [
      	'email' => $email,
      	'password' => $password
  		]
		]);
		$status = json_decode($res->getbody())->status;
		if($status == 'OK'){
			$token = json_decode($res->getbody())->token;
			Session::put('token',$token);
			return redirect('profile');
		}else{
			Session::flash('message-error', 'Email and password didnt match');
			return redirect('login');
		}



	}

	public function logout()
	{
		Session::flush();
    return redirect('login');
	}
}
