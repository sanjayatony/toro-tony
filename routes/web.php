<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', 'UsersController@profile');
Route::get('/index', 'UsersController@profile');

Route::get('/login', 'UsersController@login');
Route::post('/login', 'UsersController@checkLogin');

Route::get('/profile', 'UsersController@profile');
Route::get('/change-password', 'UsersController@changePassword');

Route::post('/update', 'UsersController@update');
Route::post('/updatePassword', 'UsersController@updatePassword');

Route::get('/logout', 'UsersController@logout');
